
(function () {


	function inicializarTooltips() {

		var tooltips = $('.tooltip');

		tooltips.on('mouseenter mouseleave', function (event) {
			$(this).find('.tooltip-content').toggleClass('toggle');
		});

	}

	inicializarTooltips();

})()

/*
var tooltip = $('.tooltip');
var icono = tooltip.find('.tooltip-icon');
var contenido = tooltip.find('.tooltip-content');

console.log(icono);

icono.on('mouseenter', function () {
})

icono.on('mouseleave', function () {
	contenido.hide();	
});

//mouseover , mouseout


*/