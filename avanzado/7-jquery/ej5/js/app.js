/*  

EJERCICIO N°5

	Crear un formulario donde 1 input tenga un tooltip!

*/

(function () {

	var $tooltip = $('.tooltip');
	var $icon = $tooltip.find('.tooltip-icon');
	var $content =  $tooltip.find('.tooltip-content');

	//agregar la funcionalidad del hover.

	$icon.on('mouseenter mouseout', function () {
		$content.toggleClass('toggle');
	})

})()