/*  

EJERCICIO N°6

	Crear un componente con 3 radio buttons que hagan cambiar de color al auto

*/

var autoComponentes = $('.color-component');

autoComponentes.each(function (indice, componente) {

	var botones = $(componente).find('input[type="radio"]');

	botones.change(function ( event ) {

		var imagen = $(componente).find('.image-container img');
		var nombreImagen = this.value;

		console.log(this);
		console.log(nombreImagen);

		imagen.fadeOut("slow", function() {
	    
	    	imagen.attr('src', 'img/' + nombreImagen + '.png' );
	    	imagen.fadeIn();
	  	
	  	})

	});



})



