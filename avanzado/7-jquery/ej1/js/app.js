/*  

EJERCICIO N°1

Jquery Baby!!
Empecemos a reconocer los selectores de jQuery con un pequeño ejercicio:
- Importar la libreria de jquery
- Agregar la clase "pod" a todos los li
- Agregar la clase Simpson al h1 y a los h2 de cada pod
- Agregar a Maggie al final de la lista (append,prepend, after, before  )

	<li class="pod">
		<h2>Maggie</h2>
		<img src="img/maggie.png">
		<p class="description">
			Margaret Evelyn "Maggie" Simpson es un personaje ficticio de la serie de televisión de dibujos animados Los Simpson. Es la tercera hija del matrimonio protagonista, Homer y Marge Simpson, y la más joven de ellos. Sus hermanos mayores son Bart y Lisa Simpson. Siempre se la ve succionando un chupete y cuando camina, suele tropezar con el mono y cae de frente.
		</p>							
	</li>


- Mover a bart a la primera posicion de la lista
- Ocultar los personajes cuando hacemos click en ellos.

*/


// Seleccionar un elemento en JS puro

var title = document.querySelector('h1')

// Seleccionar un elemento en jQuery

var $title = $('h1')

// Agregar una clase en JS puro

title.classList.add('simpsons-font')

// Agregar una clase en jQuery

$title.addClass('simpsons-font')

// Agregar un evento en JS puro

title.addEventListener('click',function() {
	console.log('clickeado!')
})

// Agregar un evento en jQuery

$title.click(function(){
	console.log('click!!')
})

// 