/*  

EJERCICIO N°8

	- Cargar el header dependiendo de el tamaño de la pantalla
	- En caso de hacer un resize de la pantalla se deberá cargar la imagen correspondiente.
	- Centrar el heacer horizontalmente en todo momento.

*/

var imagen = $('#header-image');


function getBreakpoint() {

	var breakpoint;
	var ancho = $(window).width();

	if ( ancho  < 768 ) {
		breakpoint = 'mobile';
	} else if ( ancho < 959 ) {
		breakpoint = 'tablet';
	} else {
		breakpoint = 'desktop';
	}

	return breakpoint;

}



function cambiarImagenDelHeader (breakpoint) {

	if ( breakpoint  === 'mobile') {
		imagen.attr('src', 'img/chavo-768.png');
	} else if ( breakpoint  === 'tablet' ) {
		imagen.attr('src', 'img/chavo-1200.png');
	} else {
		imagen.attr('src', 'img/chavo-1920.png');
	}

}


var breakpoint = getBreakpoint();
cambiarImagenDelHeader(breakpoint);

$(window).resize(function () {

	console.log($(window).width());
	console.log(getBreakpoint());

	var nuevoBreakpoint = getBreakpoint();

	if ( nuevoBreakpoint != breakpoint ) {

		breakpoint = nuevoBreakpoint;
		cambiarImagenDelHeader(nuevoBreakpoint);

	}

});

